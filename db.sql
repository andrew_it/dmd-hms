CREATE TABLE SystemUser (
	user_id INT PRIMARY KEY,
	login varchar(30) NOT NULL,
	password varchar(30) NOT NULL,
	access_level INT NOT NULL
);

CREATE TABLE HotelAdmin (
	person_id INT PRIMARY KEY,
	user_id INT NOT NULL REFERENCES SystemUser (user_id),
	first_name varchar(50) NOT NULL,
	last_name varchar(50) NOT NULL,
	phone_number varchar(50) NOT NULL
);

CREATE TABLE Hotel (
	hotel_id INT PRIMARY KEY,
	adress varchar(50) NOT NULL,
	name varchar(50) NOT NULL,
	stars INT NOT NULL,
	description varchar(150) NOT NULL,
	owner_id INT NOT NULL REFERENCES HotelAdmin (person_id)
);

CREATE TABLE RoomConfiguration (
	config_id INT PRIMARY KEY,
	single_bed INT,
	double_bed INT,
	sofa_bed INT
);

CREATE TABLE Customer (
	person_id INT PRIMARY KEY,
	user_id INT NOT NULL REFERENCES SystemUser (user_id),
	first_name varchar(50) NOT NULL,
	last_name varchar(50) NOT NULL,
	phone_number varchar(50) NOT NULL,
	payment_info varchar(50) NOT NULL
);

CREATE TABLE SystemAdmin (
	person_id INT PRIMARY KEY,
	user_id INT NOT NULL REFERENCES SystemUser (user_id),
	first_name varchar(50) NOT NULL,
	last_name varchar(50) NOT NULL,
	phone_number varchar(50) NOT NULl
);

CREATE TABLE Transaction (
	transaction_id INT PRIMARY KEY,
	person_id INT NOT NULL REFERENCES Customer (person_id),
	payment_method varchar(100) NOT NULL,
	amount FLOAT NOT NULL
);

CREATE TABLE RoomOption (
	option_id INT PRIMARY KEY,
	is_bathroom BOOLEAN NOT NULL,
	is_tv BOOLEAN NOT NULL,
	is_wifi BOOLEAN NOT NULL,
	is_bathhub BOOLEAN NOT NULL,
	is_airconditioniring BOOLEAN NOT NULL
);

CREATE TABLE Room (
	room_id INT PRIMARY KEY,
	hotel_id INT NOT NULL REFERENCES Hotel (hotel_id),
	config_id INT NOT NULL REFERENCES RoomConfiguration (config_id),
	option_id INT NOT NULL REFERENCES RoomOption (option_id),
	quantity INT NOT NULL,
	title varchar(100) NOT NULL,
	description varchar(150) NOT NULL,
	cost FLOAT NOT NULL
);

CREATE TABLE Booking (
	room_id INT NOT NULL REFERENCES Room (room_id),
	person_id INT NOT NULL REFERENCES Customer (person_id),
	transaction_id INT NOT NULL REFERENCES Transaction (transaction_id),
	quantity INT NOT NULL,
	checkin_date TIME NOT NULL,
	checkout_date TIME NOT NULL
);

