from flask_wtf import FlaskForm
from wtforms import BooleanField, StringField, TextField, FloatField, TextAreaField
from wtforms.validators import InputRequired, ValidationError


class LoginForm(FlaskForm):
    openid = StringField('openid', validators = [InputRequired()])
    remember_me = BooleanField('remember_me', default=False)

class IndexForm(FlaskForm):
    pass